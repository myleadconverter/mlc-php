<?php

require_once('Helpers.php');
require_once('JSON.php');


final class MyLeadConverter {

    /**
     * @var string The MyLeadConverter account's api id.
     */
    public $id = '';

    /**
     * @var string The MyLeadConverter account's api token.
     */
    public $token = '';


    public function MyLeadConverter() {}

    /**
     * Create a new "connection" to MyLeadConverter.
     * @static
     * @param string $id The MyLeadConverter account's api id.
     * @param string $token The MyLeadConverter account's api token.
     * @return MyLeadConverter
     */
    public static function connect( $id, $token ) {
        $mlc = new MyLeadConverter();
        $mlc->id = $id;
        $mlc->token = $token;
        return $mlc;
    }

    /**
     * @param string $fieldset The MyLeadConverter fieldset to which this lead will be POSTed.
     * @param array $vars The lead data. Likely just the $_POST array.
     * @return object
     *
     * $mlc = MyLeadConverter::connect(MLC_API_ID, MLC_API_TOKEN);
     * $mlc->newLead(MLC_FIELDSET_1, $_POST);
     */
    public function newLead( $fieldset, $vars ) {
        return Lead::getInstance(new Client($this->id, $this->token))->send($fieldset, $vars);
    }

    /**
     * @param string $fieldset The MyLeadConverter fieldset to which this lead will be POSTed.
     * @param string $leadId The unique id from the already saved lead in MyLeadConverter.
     * Get this from the send method of {@link Lead}.
     * @param array $vars The lead data. Likely just the $_POST array.
     * @return object
     *
     * $mlc = MyLeadConverter::connect(MLC_API_ID, MLC_API_TOKEN);
     * if ($formStep == '1') {
     *     $_SESSION['mlcLeadId'] = $mlc->newLead(MLC_FIELDSET_1, $_POST)->uniqueId;
     * } else if ($formStep == '2') {
     *     $mlc->updateLead(MLC_FIELDSET_2, $_SESSION['mlcLeadId'], $_POST);
     * }
     */
    public function updateLead( $fieldset, $leadId, $vars ) {
        return Lead::getInstance(new Client($this->id, $this->token))->update($leadId, $fieldset, $vars);
    }

    /**
     * Send a lead to MyLeadConverter via email. For servers without cURL.
     * @param string $to The lead source/account email address.
     * @param array $vars The lead data.
     * @return bool
     */
    public function sendLeadViaEmail( $to, $vars ) {
        return EmailClient::send($to, $vars);
    }

    /**
     * Generate an HTML <select> for displaying products/services as specified in MyLeadConverter.
     * @param string $name The <select> name.
     * @return string The generated HTML string.
     */
    public function productsSelect( $name ) {
        return ProductsServices::select($name, new Client($this->id, $this->token));
    }

    /**
     * @param string $name The checkbox group name.
     * @param array $wrap Wrap each checkbox with some HTML. 0:before, 1:after
     * @param string $template The template to use for rendering each checkbox.
     * @return array
     */
    public function productsCheckboxes(
        $name,
        $wrap = array('',''),
        $template = '<label><input name="%s" value="%s" type="checkbox"> %s</label>'
    ) {
        return ProductsServices::checkboxGroup($name, new Client($this->id, $this->token), $wrap, $template);
    }

}


final class MLC {

    // cookie names
    const COOKIE_NAME       = '__mlc'; // set by www.myleadconverter.com/mlc.js
    const DATA_COOKIE_NAME  = '__xmlc';

    // cookie keys
    const _LANDING_     = 'mlc__landing'; // the first page visited
    const _REFERRER_    = 'mlc__referrer'; // referrer (from HTTP referer header)
    const _LANGUAGE_    = 'mlc__language'; // user's browser language setting
    const _IP_ADDRESS_  = 'mlc__ip'; // user ip address (no guarantees this is correct - primarily used for (black/grey)listing)
    const _USER_AGENT_  = 'mlc__ua'; // user agent string
    const _UTMZ_        = 'ga__utmz'; // google analytics
    const _UTMA_        = 'ga__utma'; // google analytics


    /**
     * This is a static class that cannot be instantiated.
     * @throws Exception
     */
    public function MLC() {
		throw new Exception('This class cannot be instantiated.');
	}

    /**
     * Retrieve the current user's stats/analytics from the MyLeadConverter cookie as set by mlc.js.
     * @static
     * @return array
     */
    public static function stats() {
		$userData = array();
        parse_str(isset($_COOKIE[self::COOKIE_NAME]) ? $_COOKIE[self::COOKIE_NAME] : '', $userData);
		return $userData;
	}

    /**
     * Store and retrieve data from a cookie.
     * @static
     * @param string $key
     * @param string $value
     * @return array|string
     */
    public static function data() {
        if (func_num_args() == 0) {
            // retrieve data
            return self::decomposeData();
        } elseif (func_num_args() == 1) {
            // retrieve data for key
            $key = func_get_arg(0);
            $arr = self::decomposeData();
            return $arr[$key];
        } elseif (func_num_args() == 2) {
            // set key = value and return the entire array from cookie
            $key = func_get_arg(0);
            $value = func_get_arg(1);
            $arr = self::decomposeData();
            $arr[$key] = $value;
            setcookie(self::DATA_COOKIE_NAME, base64_encode(Helpers::query_stringify($arr)), time() + (60 * 60 * 24), '/');
            return $arr;
        } else {
            return array();
        }
    }

    /**
     * Get the data cookie's value as an array.
     * @static
     * @return array
     */
    private static function decomposeData() {
        $arr = array();
        $val = isset($_COOKIE[self::DATA_COOKIE_NAME]) ? base64_decode($_COOKIE[self::DATA_COOKIE_NAME]) : '';
        parse_str($val, $arr);
        return $arr;
    }

}


final class Lead extends Resource {

    /**
     * @var Client|null The {@link Client} object to be used for requests to the MyLeadConverter API.
     */
    private $client = null;

    /**
     * @var string The name of the api resource.
     */
    public $resourceName = 'leads';

    /**
     * @var string Serialization format.
     */
    public $accept = parent::JSON_RESPONSE;

    /**
     * @var string|null The raw response body (xml or json string).
     */
    public $raw = null;

    /**
     * @var object|null The parsed response body.
     */
    public $parsed = null;


    /**
     * @param string $apiId The MyLeadConverter account's api id.
     * @param string $apiToken The MyLeadConverter account's api token.
     * @param Client $_client A {@link Client} instance for sending requests to the MLC api via HTTP.
     */
    public function Lead( $apiId, $apiToken, Client $_client = null ) {
        if (is_null($_client)) $this->client = new Client($apiId, $apiToken, $this->accept);
        else $this->client = $_client;
    }

    /**
     * @static
     * @param Client $_client A {@link Client} instance for sending requests to the MLC api via HTTP.
     * @return Lead The newly created lead.
     */
    public static function getInstance( Client $_client ) {
        $cred = $_client->credentials();
        return new Lead($cred['id'], $cred['token'], $_client);
    }

    /**
     * Post to MyLeadConverter
     * @param string $mlcFieldsetId Fieldset's unique identifier.
     * @param array $postVars Reference to $_POST. Any modifications to user-submitted data should be done prior to construction of lead
     * @return object
     */
    public function send( $mlcFieldsetId, $postVars ) {
    	$postVars['mlc__fieldset'] = $mlcFieldsetId;
    	$result = @$this->client->post($this->resourceName, $postVars + MLC::stats());
    	$this->raw = $result['raw'];
    	$this->parsed = $result['parsed'];
    	return $this->parsed;
    }

    /**
     * Update an existing lead
     * @param string $mlcLeadId The lead's id.
     * @param string $mlcFieldsetId A fieldset's unique identifier.
     * @param array $postVars Reference to $_POST. Any modifications to user-submitted data should be done prior to construction of lead.
     * @return object
     */
    public function update( $mlcLeadId, $mlcFieldsetId, $postVars ) {
    	$postVars['mlc__fieldset'] = $mlcFieldsetId;
    	$result = @$this->client->post("$this->resourceName/$mlcLeadId", $postVars + MLC::stats());
    	$this->raw = $result['raw'];
    	$this->parsed = $result['parsed'];

    	return $this->parsed;
    }
}


final class ProductsServices extends Resource {

    /**
     * @var string Serialization format.
     */
    public $accept = parent::JSON_RESPONSE;


	public function ProductsServices() {}

    /**
     * Retrieve products/services from MLC.
     * @static
     * @param Client $_client A {@link Client} for sending HTTP requests to MyLeadConverter.
     * @return array|bool An array of product objects
     *
     * [ {"PRODUCT_ID":"PRODUCT_NAME"}, {"PRODUCT_ID":"PRODUCT_NAME"} ]
     */
    public static function get( Client $_client ) {
		try {
			$productsServices = $_client->get('products-services');
			return $productsServices['parsed'];
		} catch (Exception $e) {
			return array();
		}
	}

    /**
     * Create a checkbox group containing the products/services as entered in MyLeadConverter.
     * @static
     * @param string $groupName The name to be applied to each checkbox input.
     * @param Client $_client The api client responsible for sending HTTP requests to MyLeadConverter.
     * @param array $wrap Wrap each checkbox with some HTML. 0:before, 1:after
     * @param string $template The template to use for rendering each checkbox.
     * @return array The checkboxes as an array.
     */
    public static function checkboxGroup(
		$groupName,
		Client $_client,
		$wrap = array('', ''),
		$template = '<label><input name="%s" value="%s" type="checkbox"> %s</label>'
	) {
		$productsServices = self::get($_client);
		$html = array();
		foreach ($productsServices as $productId => $productName) {
			$html[] = $wrap[0] . sprintf($template, $groupName, $productId, $productName) . $wrap[1];
		}
		return $html;
	}

    /**
     * Create an HTML <select> element containing the products/services as entered in MyLeadConverter.
     * @static
     * @param string $selectName The name of the select.
     * @param Client $_client The api client responsible for sending HTTP requests to MyLeadConverter.
     * @return string The HTML string.
     */
    public static function select( $selectName, Client $_client ) {
		$productsServices = self::get($_client);
		$html = "<select name=\"$selectName\">";
		foreach ($productsServices as $productId => $productName) {
            $html .= "<option value=\"$productId\">$productName</option>";
		}
		$html .= "</select>";
		return $html;
	}

}


abstract class Resource {
    const JSON_RESPONSE = 'application/json';
	const XML_RESPONSE = 'application/xml';

    /**
     * @var string Determines response format. (HTTP Accept Header)
     */
    public $accept;
}


final class Client {

	const BASE_URL = 'https://www.myleadconverter.com/';
	const UA = 'php-mlc';
	const DEBUG = false;

    /**
     * @var string The API version we're targeting.
     */
    private $_apiVersion = '1.0';

    /**
     * @var string The API revision we're targeting.
     */
    private $_apiRevision = '011712';

    /**
     * @var null|TinyHttp The HTTP client used to send requests to the MyLeadConverter API.
     */
    private $_http  = null;

    /**
     * @var string Determines response format. (HTTP Accept Header)
     */
    private $_accept = 'application/json';


    /**
     * @param string $apiId The MyLeadConverter account's api id.
     * @param string $apiToken The MyLeadConverter account's api token.
     * @param string $accept The data type we're transferring to/from MyLeadConverter.
     * @param TinyHttp $http An instance of {@link TinyHttp} for sending HTTP requests.
     */
    public function Client( $apiId, $apiToken, $accept = null, $http = null ) {
		if (!is_null($accept)) $this->_accept = $accept;

		if (is_null($http)) {
			$http = new TinyHttp(
				self::BASE_URL,
				array(
					'debug' => self::DEBUG,
					'curlopts' => array(
						CURLOPT_USERAGENT => self::UA,
						CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem',
						CURLOPT_SSL_VERIFYPEER => true,
						CURLOPT_SSL_VERIFYHOST => 2
					)
				)
			);
		}
		$http->authenticate($apiId, $apiToken);
		$this->_http = $http;
	}

    /**
     * @static
     * @param string $apiId The MyLeadConverter account's api id.
     * @param string $apiToken The MyLeadConverter account's api token.
     * @param string $accept The data type we're transferring to/from MyLeadConverter.
     * @return Client The newly create {@link Client}.
     */
    public static function getInstance( $apiId, $apiToken, $accept = null ) {
		return new Client($apiId, $apiToken, $accept);
	}

    /**
     * Make a request via HTTP GET
     * @param string $path The resource path.
     * @param array $params HTTP parameters to send with request.
     * @return array|bool
     */
    public function get( $path, $params = array() ) {
		return $this->_do('get', $path, array(), $params);
	}

    /**
     * Make a request via HTTP POST
     * @param string $path The resource path.
     * @param array $params HTTP parameters to send with request.
     * @return array|bool
     */
    public function post( $path, $params = array() ) {
		return $this->_do('post', $path, array('Content-Type' => 'application/x-www-form-urlencoded'), $params);
	}

    /**
     * Make a request via HTTP DELETE
     * @param string $path The resource path.
     * @param array $params HTTP parameters to send with request.
     * @return array|bool
     */
    public function delete( $path, $params = array() ) {
        return $this->_do('delete', $path, array(), $params);
	}

    /**
     * @return array Credentials being used with requests from this client. (HTTP Basic Authentication)
     */
    public function credentials() {
		return array('id' => $this->_http->user, 'token' => $this->_http->pass);
	}

    /**
     * Make a request
     * @param string $verb Which HTTP method are we using to make the request.
     * @param string $path The resource path.
     * @param array $headers HTTP headers to send with request.
     * @param array $params HTTP parameters to send with request.
     * @return array|bool
     */
    private function _do( $verb, $path, $headers, $params ) {
        $path = "/api/$this->_apiVersion/$path";
        $headers = $headers + array(
        	'X-API-Version' => $this->_apiRevision,
        	'Accept' => $this->_accept
        );
        return $this->_process(call_user_func_array(
            array($this->_http, $verb),
            array($path, $headers, empty($params) ? '' : Helpers::query_stringify($params))
        ));
    }

    /**
     * @param array $response The response as received by {@link TinyHttp} after request to MyLeadConverter.
     * @return array|bool
     * @throws Exception
     */
    private function _process( $response ) {
		list($status, $headers, $body) = $response;
		if ($status == 204) { return true; }
		if (empty($headers['Content-Type'])) { throw new Exception('Response header is missing Content-Type'); }
		$contentType = explode(';', $headers['Content-Type']);
		$contentType = trim($contentType[0]);
		switch ($contentType) {
			case 'application/json' :
				return array(
					'raw' => $body,
					'parsed' => $this->_toJson($status, $headers, $body)
				);
				break;
			case 'text/xml' :
				return array(
					'raw' => $body,
					'parsed' => $this->_toXml($status, $headers, $body)
				);
				break;
		}
		throw new Exception('Unexpected content type: ' . $contentType);
	}

	private function _toJson( $status, $headers, $body ) {
		if (function_exists('json_decode')) {
			$decoded = json_decode($body);
		} else {
			$json = new Services_JSON();
			$decoded = $json->decode($body);
		}
		if (200 <= $status && $status < 300) { return $decoded; }
		throw new RestException(
			isset($decoded->code) ? $decoded->code : null,
			$decoded->message,
			isset($decoded->description) ? $decoded->description : null
		);
	}

	private function _toXml( $status, $headers, $body ) {
		$decoded = simplexml_load_string($body);
		if (200 <= $status && $status < 300) { return $decoded; }
		throw new RestException(
			(string)$decoded->Code,
			(string)$decoded->Message,
			(string)$decoded->Description
		);
	}

}


/**
 * Based on TinyHttp from https://gist.github.com/618157.
 * Copyright 2011, Neuman Vong. BSD License.
 */
class TinyHttp {

    var $user, $pass, $scheme, $host, $port, $debug, $curlopts;

    public function TinyHttp( $uri = '', $kwargs = array() ) {
        foreach (parse_url($uri) as $name => $value) $this->$name = $value;
        $this->debug = isset($kwargs['debug']) ? !!$kwargs['debug'] : NULL;
        $this->curlopts = isset($kwargs['curlopts']) ? $kwargs['curlopts'] : array();
    }

    public function __call( $name, $args ) {
        list($res, $req_headers, $req_body) = $args + array(0, array(), '');

        $opts = $this->curlopts + array(
            CURLOPT_URL => "$this->scheme://$this->host$res",
            CURLOPT_HEADER => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_INFILESIZE => -1,
            CURLOPT_POSTFIELDS => NULL,
            CURLOPT_TIMEOUT => 60,
        );

        foreach ($req_headers as $k => $v) $opts[CURLOPT_HTTPHEADER][] = "$k: $v";
        if ($this->port) $opts[CURLOPT_PORT] = $this->port;
        if ($this->debug) $opts[CURLINFO_HEADER_OUT] = TRUE;
        if ($this->user && $this->pass) $opts[CURLOPT_USERPWD] = "$this->user:$this->pass";
        switch ($name) {
            case 'get':
                $opts[CURLOPT_HTTPGET] = TRUE;
                break;
            case 'post':
                $opts[CURLOPT_POST] = TRUE;
                $opts[CURLOPT_POSTFIELDS] = $req_body;
                break;
            case 'put':
                $opts[CURLOPT_PUT] = TRUE;
                if (strlen($req_body)) {
                    if ($buf = fopen('php://memory', 'w+')) {
                        fwrite($buf, $req_body);
                        fseek($buf, 0);
                        $opts[CURLOPT_INFILE] = $buf;
                        $opts[CURLOPT_INFILESIZE] = strlen($req_body);
                    } else throw new TinyHttpException('unable to open temporary file');
                }
                break;
            case 'head':
                $opts[CURLOPT_NOBODY] = TRUE;
                break;
            default:
                $opts[CURLOPT_CUSTOMREQUEST] = strtoupper($name);
                break;
        }
        try {
            if ($curl = curl_init()) {
                if (curl_setopt_array($curl, $opts)) {
                    if ($response = curl_exec($curl)) {
                        $parts = explode("\r\n\r\n", $response, 3);
                        list($head, $body) = ($parts[0] == 'HTTP/1.1 100 Continue')
                            ? array($parts[1], $parts[2])
                            : array($parts[0], $parts[1]);
                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        if ($this->debug) {
                            error_log(
                                curl_getinfo($curl, CURLINFO_HEADER_OUT) .
                                    $req_body
                            );
                        }
                        $header_lines = explode("\r\n", $head);
                        array_shift($header_lines);
                        foreach ($header_lines as $line) {
                            list($key, $value) = explode(":", $line, 2);
                            $headers[$key] = trim($value);
                        }
                        curl_close($curl);
                        if (isset($buf) && is_resource($buf)) fclose($buf);
                        return array($status, $headers, $body);
                    } else throw new TinyHttpException(curl_error($curl));
                } else throw new TinyHttpException(curl_error($curl));
            } else throw new TinyHttpException('unable to initialize cURL');
        } catch (ErrorException $e) {
            if (is_resource($curl)) curl_close($curl);
            if (isset($buf) && is_resource($buf)) fclose($buf);
            throw $e;
        }
    }

    public function authenticate( $user, $pass ) {
        $this->user = $user;
        $this->pass = $pass;
    }

}


final class EmailClient {

    public function __construct() {
        throw new Exception('cannot instantiate a new instance of static class EmailClient.');
    }

    /**
     * @static
     * @param string $to The lead source/account email address.
     * @param array $vars The lead data.
     * @return bool
     */
    public static function send( $to, $vars ) {
        $headers = array(
            "MIME-Version: 1.0",
            "Content-Type: text/plain; charset=utf-8",
            "Content-Transfer-Encoding: 8bit",
            "From: EmailClient <EmailClient@myleadconverter.com>",
            "X-Mailer: PHP/".phpversion()
        );

        return mail(
            $to,
            "=?utf-8?B?".base64_encode('new lead')."?=",
            Helpers::query_stringify($vars + MLC::stats()),
            implode("\r\n", $headers)
        );
    }

}


class TinyHttpException extends ErrorException {}


class RestException extends Exception {

	protected $description;

	public function RestException( $code, $msg, $description = '' ) {
		$this->description = $description;
		parent::__construct($msg, $code);
	}

	public function getDescription() {
		return $this->description;
	}

}