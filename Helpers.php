<?php

require_once('JSON.php');

final class Helpers {
	
	/**
	 * From: http://www.nyphp.org/PHundamentals/5_Storing-Data-Submitted-Form-Displaying-Database
	 */
	public static function fix_magic_quotes($var = NULL, $sybase = NULL) {
		// if sybase style quoting isn't specified, use ini setting
		if (!isset($sybase)) $sybase = ini_get('magic_quotes_sybase');

		// if no var is specified, fix all affected superglobals
		if (!isset($var)) {
			// if magic quotes is enabled
			if (get_magic_quotes_gpc()) {
				// workaround because magic_quotes does not change $_SERVER['argv']
				$argv = isset($_SERVER['argv']) ? $_SERVER['argv'] : NULL; 

				// fix all affected arrays
				foreach (array('_ENV', '_REQUEST', '_GET', '_POST', '_COOKIE', '_SERVER') as $var) {
					$GLOBALS[$var] = self::fix_magic_quotes($GLOBALS[$var], $sybase);
				}

				$_SERVER['argv'] = $argv;

				// turn off magic quotes, this is so scripts which
				// are sensitive to the setting will work correctly
				ini_set('magic_quotes_gpc', 0);
			}

			// disable magic_quotes_sybase
			if ($sybase) ini_set('magic_quotes_sybase', 0);

			// disable magic_quotes_runtime
			set_magic_quotes_runtime(0);
			return TRUE;
		}

		// if var is an array, fix each element
		if (is_array($var)) {
			foreach ($var as $key => $val) {
				$var[$key] = self::fix_magic_quotes($val, $sybase);
			}

			return $var;
		}

		// if var is a string, strip slashes
		if (is_string($var)) {
			return $sybase ? str_replace('\'\'', '\'', $var) : stripslashes($var);
		}

		// otherwise ignore
		return $var;
	}

	/**
	 * Get the current user's IP address.
	 * @return String IP address if found. Otherwise, empty string.
	 */
	public static function get_ip_address() {
		$keys = array(
			'HTTP_CLIENT_IP',
			'HTTP_X_FORWARDED_FOR',
			'HTTP_X_FORWARDED',
			'HTTP_X_CLUSTER_CLIENT_IP',
			'HTTP_FORWARDED_FOR',
			'HTTP_FORWARDED',
			'REMOTE_ADDR'
		);
		// loop through possible ip addresses
		foreach ($keys as $key) {
			// make sure the key exists to avoid NPE
			if (array_key_exists($key, $_SERVER)) {
				// split -> trim each value -> validate ip address -> only valid ips will pass through to the $ipArray variable
				$ipArray = array_filter(array_map('trim', explode(',', $_SERVER[$key])), array(__CLASS__, 'valid_ip_address'));
				// return the first ip address if found. http://en.wikipedia.org/wiki/X-Forwarded-For
				if (!empty($ipArray)) { return $ipArray[0]; }
			}
		}
		// no valid ip found; return an empty string.
		return '';
	}

	/**
	 * Check that the submitted value is a valid ip address
	 * @param $ip string Possible ip address string.
	 * @return boolean Valid IP address?
	 */
	public static function valid_ip_address( $ip ) {
		if (function_exists('filter_var')) {
			return (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false);
		}
		// fallback
		return preg_match('/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/', $ip) == 1;
	}

	/**
	 * Get the current user's browser language
	 * @return String The language string.
	 */
	public static function get_lang() {
		$lang_choice = '';

		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			// up to the first delimiter
			preg_match("/([^,;]*)/", $_SERVER['HTTP_ACCEPT_LANGUAGE'], $langs);
			$lang_choice = strtolower($langs[0]);
		}
		
		return $lang_choice;
	}

	/**
	 * Get the user agent string.
	 * @return String
	 */
	public static function get_user_agent() {
		return array_key_exists('HTTP_USER_AGENT', $_SERVER) ? $_SERVER['HTTP_USER_AGENT'] : '';
	}

	/**
	 * Get the referrer url.
	 * @return String
	 */
	public static function get_referrer() {
		return array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '';
	}

	/**
	 * Get Google Analytics cookies
	 * @return Array
	 */
	public static function get_ga_cookies() {
		$_cookies = array();
		if (array_key_exists('__utma', $_COOKIE)) {
			$_cookies['ga__utma'] = $_COOKIE['__utma']; // visitor
		}
		if (array_key_exists('__utmz', $_COOKIE)) {
			$_cookies['ga__utmz'] = $_COOKIE['__utmz']; // campaign
		}
		return $_cookies;
	}

	/**
	 * Redirect via Location header.
	 * @param string $pg Path to webpage on this server.
	 * @return void
	 */
	public static function redirectTo( $pg ) {
		header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . $pg);
		die();
	}

	/**
	 * Check to see that cookies are enabled.
	 * @return boolean
	 */
	public static function cookies_enabled() {
		if (!isset($_SESSION['cookiesEnabled'])) {
			if (setcookie('__null', 'null', time() + 60, '/')) {
				if (isset($_COOKIE['__null'])) {
					$_SESSION['cookiesEnabled'] = true;
					return true;
				}
			}
			$_SESSION['cookiesEnabled'] = false;
			return false;
		} else {
			return (boolean)$_SESSION['cookiesEnabled'];
		}
	}

	/**
	 * Get the current absolute url
	 * @param boolean $withParams Include query parameters?
     * @return string
	 */
	public static function get_current_url( $withParams = true ) {
		$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
		if ($withParams) $url .= '?' . $_SERVER['QUERY_STRING'];
		return $url;
	}

    /**
     * Glorified version of php's own http_build_query for use with languages that understand field groups
     * without syntactic wizardry (ie. <input type="checkbox" name="myCheckboxGroup[]">).
     * Will transform v[0]=value1&v[1]=value2 to v[]=value1&v[]=value2;
     *
     * @link http://www.php.net/manual/en/function.http-build-query.php#78603
     */
    public static function query_stringify( $postVars ) {
    	$q = http_build_query($postVars, '', '&');
    	return preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '[]=', $q);
    }

    /**
     * @static
     * @param string $str The JSON string to decode.
     * @param bool $asArray Return JSON as an associative array?
     * @return mixed The decoded JSON object/number/string/etc.
     */
    public static function decode_json( $str, $asArray = false ) {
        if (function_exists('json_decode')) {
        	return json_decode($str, $asArray);
        } else {
        	$json = new Services_JSON( $asArray ? 16 : 0 );
        	return $json->decode($str);
        }
    }

    /**
     * @static
     * @param object $obj The object to encode as JSON.
     * @return mixed|string
     */
    public static function encode_json( $obj ) {
        if (function_exists('json_decode')) {
            return json_encode($obj);
        } else {
        	$json = new Services_JSON();
            return $json->encode($obj);
        }
    }
}
