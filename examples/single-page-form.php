<?php

require_once('../mlc.php');
require_once('../Helpers.php');

session_start();

Helpers::fix_magic_quotes();

define('MLC_API_ID', 'account id from myleadconverter.com');
define('MLC_API_TOKEN', 'api token from myleadconverter.com');
define('MLC_FIELDSET_1', 'fieldset id from myleadconverter.com');

$mlc = MyLeadConverter::connect(MLC_API_ID, MLC_API_TOKEN);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $mlc->newLead(MLC_FIELDSET_1, $_POST);
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Test Form</title>
	</head>
	<body>
	<form name="frmrequest" id="frmrequest" action="Example.php" method="post">
		<ul>
			<li style="margin-top:8px;"><input name="ZipCode" id="ZipCode" value="Zip Code" type="text" class="input_field"/></li>
			<li><input name="relationindividual" id="relationindividual" value="Relation to individual" type="text" class="input_field"/></li>
			<li><input name="YourName" id="YourName" value="Your Name" type="text" class="input_field"/></li>
			<li><input name="Email" id="Email" value="Email" type="text" class="input_field"/></li>
			<li><input name="Phone" id="Phone" value="Phone" type="text" class="input_field"/></li>
			<li><input name="timetoreach" id="timetoreach" value="Best Time to Reach you" type="text" class="input_field"/></li>
			<li><input name="custom1" value="custom 1" type="text" /></li>
			<li><input name="marketingBudget" value="budget" type="text" /></li>
			<?php
			echo implode("\n", $mlc->productsCheckboxes('service[]', array('<li>','</li>')));
			?>
		    <li><input name="" type="image" src="images/submit-btn.jpg" class="fright" style="margin:7px 32px 0 0;" /></li>
		</ul>
	</form>
	<?php /* Include LeadStats javascript on EVERY PUBLIC PAGE */ ?>
	<script type="text/javascript">
		(function(d, w, t) {
			var m = d.createElement(t), s = d.getElementsByTagName(t)[0];
			m.src = '//www.myleadconverter.com/mlc.js?d='+w.location.href;
			s.parentNode.insertBefore(m,s);
		}(document, window, 'script'));
	</script>
	</body>
</html>
